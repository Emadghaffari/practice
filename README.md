GITLAB CI/CD

    stack file:

    install DOCKER
    https://get.docker.com/

    sudo usermod -aG docker bret

    https://docs.docker.com/machine/install-machine/
    	docker-machine --version

    https://github.com/docker/compose/releases

    sudo -i
    curl -L https://github.com/docker/compose/releases/download/1.25.4/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
    chmod +x /usr/local/bin/docker-compose

docker stack deploy -c stack.yml --with-registry-auth NAME

create registry in gitlab and build image in server....

    docker push registry.gitlab.com/emadghaffari/practice

create gitlab-runner with docker image

    create a gitlab-runner
    	docker run -d --name gitlab-runner --restart always \
    	     -v /srv/gitlab-runner/config:/etc/gitlab-runner \
    	     -v /var/run/docker.sock:/var/run/docker.sock \
    	     gitlab/gitlab-runner:latest

exec in docker-runner container
docker container exec -it gitlab-runner bash

    register a gitlab-runner
    gitlab-runner register -n \
        --url https://gitlab.com/ \
        --registration-token TOKEN \
        --executor docker \
        --description "complex runner" \
        --docker-image "docker:19.03.12" \
        --docker-privileged

    register a gitlab-runner
    gitlab-runner register -n \
        --url https://gitlab.com/ \
        --registration-token TOKEN \
        --executor docker \
        --description "complex runner" \
        --docker-image "docker:19.03.12" \
        --docker-privileged

create a shepherd service for auto change the service and update with registry_auth
https://github.com/djmaze/shepherd

    docker service create --name shepherd \
        --constraint "node.role==manager" \
        --env SLEEP_TIME="1m" \
        --env BLACKLIST_SERVICES="shepherd" \
        --env WITH_REGISTRY_AUTH="true" \
        --env IMAGE_AUTOCLEAN_LIMIT="3" \
        --mount type=bind,source=/var/run/docker.sock,target=/var/run/docker.sock,ro \
        --mount type=bind,source=/root/.docker/config.json,target=/root/.docker/config.json,ro \
        mazzolino/shepherd
